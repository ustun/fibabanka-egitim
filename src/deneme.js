// @ts-check

/**
 *
 *
 * @param {number} a
 * @param {number} b
 * @returns
 */
function topla(a, b) {
  return a + b;
}

var x = String(topla(1, 2));

console.log(x.toFixed());
