import {
  Customer,
  BankAccount,
  Money,
  Currency,
  CurrencyMismatchException,
  FooException
} from "./Bank";

describe("Basic bank functionality", () => {
  describe("customer", () => {
    it("should be created with tc and name and surname", () => {
      let c1 = new Customer(312, "Ustun", "Ozgur");

      expect(c1.name).toBe("Ustun");
    });

    test("a new customer should have zero accounts", () => {
      let c1 = new Customer(312, "Ustun", "Ozgur");

      expect(c1.nAccounts()).toBe(0);

      expect(c1.tc).toBe(312);
    });
  });

  describe("account", () => {
    let c1: Customer, c2: Customer;

    beforeEach(() => {
      c1 = new Customer(312, "Ustun", "Ozgur");
    });

    beforeAll(() => {
      c2 = new Customer(312, "Ustun", "Ozgur");
    });

    it("should create a new account with customer and currency", () => {
      let a1 = new BankAccount(c1, Currency.USD);
      let a2 = new BankAccount(c2, Currency.TL);
      expect(a1).toBeTruthy();

      expect(c1.nAccounts()).toBe(1);
    });

    it("should not create a new account with unknown currency", () => {
      //  expect(() => new BankAccount(c1, "TL")).toThrow();
    });

    it("before each ve before all farki", () => {
      expect(c1.nAccounts()).toBe(0);
      expect(c2.nAccounts()).toBe(1);
    });

    it("should be able to withdraw and deposit", () => {
      let a1 = new BankAccount(c1, Currency.TL);

      let depositResult = a1.deposit(new Money(10, Currency.TL));

      let foo: number = JSON.parse("1");

      console.log(foo.toFixed());

      if (typeof depositResult != "number") {
        console.log(depositResult.toUpperCase());
      } else {
        let x = depositResult;
        console.log(x.toFixed(1));
      }

      expect(a1.balance).toBe(10);
    });

    it("should be not able to withdraw and deposit with different currencies", () => {
      let a1 = new BankAccount(c1, Currency.TL);

      expect(() => a1.deposit(new Money(10, Currency.USD))).toThrow();
      expect(() => a1.deposit(new Money(10, Currency.USD))).toThrowError(
        CurrencyMismatchException
      );

      var e1 = expect(() => a1.deposit(new Money(10, Currency.USD)));

      var e2 = e1.not; // property

      e2.toThrowError(FooException);

      expect(() => a1.deposit(new Money(10, Currency.USD))).toThrow();
      expect(() => a1.deposit(new Money(10, Currency.USD))).toThrow();

      expect(a1.balance).toBe(0);
    });
  });
});
