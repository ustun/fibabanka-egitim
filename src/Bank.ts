export class Customer {
  nAccounts() {
    return this.accounts.length;
  }

  accounts: Array<BankAccount> = [];

  constructor(public tc: number, public name: string, public surname: string) {}
}

// type Currency = "TL" | "USD";

export enum Currency {
  TL = "TL",
  USD = "USD"
}

export class BankAccount {
  static knownCurrencies = ["TL", "USD"];

  deposit(money: Money) {
    if (this.currency != money.currency) {
      throw new CurrencyMismatchException();
    }

    this.balance = this.balance + money.amount;
    if (Date.now() > 123) {
      return 3;
    } else {
      return "ali";
    }
  }

  balance = 0;

  constructor(public customer: Customer, public currency: Currency) {
    customer.accounts.push(this);
  }
}

export class Money {
  constructor(public amount: number, public currency: Currency) {}
}

export class BankException extends Error {}
export class CurrencyMismatchException extends BankException {}

export class FooException extends BankException {}
